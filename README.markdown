# Folder Git

**This project is no longer maintained. [I'm using GNU `parallel`](https://gitlab.com/engmark/root/-/blob/2980b59bbe5de3f6f45af9763e9ae4723d5390cc/includes/.bash_history#L699-702) instead.**

[![Build Status](https://gitlab.com/victor-engmark/fgit/badges/master/build.svg)](https://gitlab.com/victor-engmark/fgit)

Run a Git command in several repositories.

## Usage

    fgit --help

For example, to run `git pull` in all repositories in your home directory:

    fgit pull -- ~/*/.git/..

## Test

    make test

## Install

    sudo make install
